
Project Review
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment       | Project Review
-------------------- | --------------------------------
Name                 |Zhisheng Qu	
Net ID               |zq315
Assigned project (#) | 17
Review due           | Monday, 4 May 11:59PM


Please write detailed comments answering the questions below.
Do not just answer "yes" or "no" - comment on *how well* the authors
address each of these aspects of experimentation, and offer 
suggestions for improvement.

## Overview

1) Briefly summarize the experiment in this project.

The project I reviewed is aim to make a comparison of packet-processing time of three different routers, XORP, OVS and Linux Kernel router, with the help of GENI lab and R script software.


2) Does the project generally follow the guidelines and parameters we have 
learned in class? 

Yes, they followed the guidelines by setting a simple topology on the GENI lab and using the control-variable method to set the unvariable number of data packets and packet size (each experienment the client transmit 1000 packets
with fixed size of 100 of each packet). They tried to configured a desired and fair experienment environment with the only variable, the delay time when packet tranmitting between ingress and egress interface among three 
different routers.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal of this experiment is making a comparison among three different routers' packet-processing time. This goal is a focus and specific goal.
Yes, the result is interesting, they tried to plot the data they collected on GENI lab with the help of R, which making the comparison and difference more obvious and persuasive.


2) Are the metric(s) and parameters chosen appropriate for this 
experiment goal? Does the experiment design clearly support the experiment goal?

I think the project members tried their best to make the most fair comparison, the only variable of this experiment is the three different types of router, XORP, OVS and Linux kernel router and they configured the same network
topology (a client, a server and different routers) and fixed data packet rate (fixed number of 1000 packets and single packet size of 100). So I can say that the metrics  and the parameters are appropriate. However, this three
different experiment I could not tell whether their operating systems are identical, the report mentioned OS and Linux, but I could not figure out each experiment is operated on which operating system. As an result, this 
experiment design basically and clearly support the goal of this experiment, but there still exist some imperfection element, the designer should take more consideration when designing this experiment to make the result more strict.

3) Is the experiment well designed to obtain maximum information with the 
minimum number of trials?

I think the experiment is well-designed. The project members tried their best to lower the number of different parameters such as the packet number, size and network topology and made them identical in three different trials.
By the way, the network design is smart, and simple, the project members used just three components, the client , server and router, to configure the topology of the network realizing the experiment. Every detailed information
such as parameter of packet size was mentioned in the report, the comparison could be made witin three simple trials just configuring and substituting the router on each trial.


4) Are the metrics selected for study the *right* metrics? Are they clear, 
unambiguous, and likely to lead to correct conclusions? Are there other 
metrics that might have been better suited for this experiment?

The metrics the project members selected were right, in different routers, they use the tcpdump and the Tshark on the router to capture the packets which going through the ingress and egress interface of the router, and the data 
calculated was processed and ploted by R, it is clear to demonstrate the difference and comparison the delay time data by transfering the data to the graph. I think these metrics they used are suitable for this experiment.


5) Are the parameters of the experiment meaningful? Are the ranges 
over which parameters vary meaningful and representative?

Yes, the parameters of the experiment is meaningful, they used the TCPdump and Tshark on the router to capture the delay time of data transmitting from the ingress to the egress side of diffrent routers. The ranges they expressed on
the R by ploting the delay time of different routers, the ranges is representative to let the reader clearly see the difference among different routers. And the transmitting rate the author repeatly stress that the packet number 
of each transmision should not set over 1470 to ensure the stabality of the network.


6) Have the authors sufficiently addressed the possibility of interactions 
between parameters?

No, they do not. The parameters the auhor provided are the network topology, the packets number and size and the operating system. However, the author just addressed the invariance of each single parameters independently
but he or she did not address the posibility of the interaction among these parameters.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate 
and realistic? 

The project members made resonable comparisons, they did not involve ratio game problem, instead, they plot the CDF of three differnet routers' delay time on R seperately, which makes the reader morey directly
tell the difference of the routers on the report. The baseline selected is approprite and realistic, which is definitely  set to zero on the plot, thus the reader could easily read the accurate delay time on each plot.



## Communicating results


1) Do the authors report the quantitative results of their experiment?

Yes, the author reported the quantitative results of the delay time of three different routers on R, and plot the result in CDF. But, in this class we did not mention the CDF in too much detial, the author should introduce 
CDF in advance.

2) Is there information given about the variation and/or distribution of 
experimental results?

Yes, the author has given the variation of the experiment results, bucause the project members were making comparison of three different routers, so there are totally three different result (in this experiment, the author ploted)
three different result of delay time of routers. The author distributed the result seperately by listing three differnt CDF plot on R to express the difference of packet-processing time of three different routers.

3) Do the authors practice *data integrity* - telling the truth about their data, 
avoiding ratio games and other practices to artificially make their results seem better?

Yes, the author practiced the data integrity, the ratio game was avoided in this experiment, the plot of the result and the conclusion of three different trials was made seperately and independently. I think in this experiment the 
author knows that making a ratio comparison of three different data is meaningless, so the author just draw the conclusion accordingly.


4) Is the data presented in a clear and effective way? If the data is presented in 
graphical form, is the type of graph selected appropriate for the "story" that 
the data is telling? 

No, I do not think that the data was presented in a clear and effective way. The author just presented three different plots on R about the delay time of three different routers and I could not say that the graph is appropriate
Because the the Y-axis represents the CDF (Cumulative distribution function), but the author did not even state the formula of CDF in advance. As an result, it is difficult for someone does not know CDF to read the gragh and 
understand the "story" directly.

5) Are the conclusions drawn by the authors sufficiently supported by the 
experiment results?

I think the the conclusion is sufficiently supported by the experiment results. Because the project members program the CDF (Cumulative distribution function) in R to calculate the distribution of the 1000 packets delay time, so
the plots generated by R is rigorous and persuasive conclusion based on the experiment data (the packet-processing time of the routers) and result.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, 
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

Yes, the author provided detailed steps on reproducing the experiment but I think the author had better introduce these three different routers in advanvce rather than providing us the raw data and code directly.
The instruction steps were clear, the author not only provide us the screenshot of each step, but also gave us extra reminder such the ssh key for windows system and the appendix of ours different netID, which is very 
considerate.


2) Were you able to successfully produce experiment results? 

Yes, I can reproduce the experiment result in Windows system.


3) How long did it take you to run this experiment, from start to finish?

About half of one hour (Except for the setup time of the VMs on GENI).


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?s 

Yes, I made some additional steps on reproducing the experiment successfully. Before I started reproducing the experiment by following the report, I review the properties of these three different routers because the author did 
not gave me the introduction on these routers in detail. Also, the extra step I made is on the ssh key. I assume the project members made the experiment on OS or Linux but my computer is Windows system, so I installed the ssh 
key on my PC in advance.


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

This experiemt should fall on the fourth degree of reproducibility. I can reproduce this experiment by following the instructions and screenshot on the report without too much thinking and effort, the report is elaborate,
so every step could be reproduces based on the code the author provided on the report. However, in light of the existence of different operating system between the author's PC and mine, I had to take more time to set up the 
intallation of the ssh key on my PC in advance.

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

Firstly I should say that this is a good experiment and detailed report, the project members design a seemed-comlicated experiment without complicated step or introduction. Their parameter and network were designed smartly,
not only satisfy the invariance of the parameters in different trials, but also keep the simpleness of the network topology to reduce the unexpected or complicated factors. However I could not say that the experiment is perfect
or flawless in general, and I would give the author some advices on the report which might be helpful. (1) The author ought to make a brief introduction on the experiment's background rather than listing the result of the 
experiment in R directly, the introduction of these three routers' background or properties should be make so that the reader could have a better understanding on the experiment. （2）There exists some redundency in this report,
I found that some code on R appears twice in this report, in both the beginning and the ending the code for configuring the graph on R are the same, I hope that the author should be more careful next time on this problem. 
(3) The results were just showed at the beginning  of this report, which making this report more like a reverse induction, besides the abrrevation of CDF should be introduced in detail such that any individual could understand
its mechanism without comfusion.










